# faial-infer: Extracts a concurrency signature from a CUDA program

`faial-infer` extracts a concurrency signature from a CUDA program. The tool
[`faial`](https://gitlab.com/umb-svl/faial) can then prove if a concurrency
signature is data-race free.




# Installation

### [Download latest `faial-infer.zip` for Linux x86-64](https://gitlab.com/umb-svl/faial-infer/-/jobs/artifacts/master/download?job=bin)

```bash
$ unzip artifact.zip
$ chmod +x faial-infer
$ ./faial-infer -h
```

# Usage

`faial-infer` expects a serialized CUDA-program, which is obtained from
[`c-to-json`](https://gitlab.com/umb-svl/c-to-json).

```bash
$ faial-infer examples/2d-example.json
```

# Development

## Configure (once)

```bash
$ python3 -m pip install poetry # Make sure you have poetry installed
$ make deps                     # Install faial-infer dependencies
```

## Add `faial-infer` to PATH

### Temporary
```bash
$ poetry shell # Places faial-infer in the PATH (not permanent)
```

### Permanent
Add this to your `.profile`:
```
$ export PATH="$PATH:$(cd /PATH/TO/faial-infer; poetry env info -p)/bin"
```

## Run tests

```bash
$ make test
```
