extern int numSteps;
int main() {
  // Easy loop
  for(int i = 0; i < 100; i++) {}

  for(int i = 0; i <= 100; i++) {}

  for(int j = numSteps; j > 0; j -= 2) {}

  for(int d = numSteps>>1; d > 0; d >>=1) {}

  int d;
  for (d =1; d <= 100; d *= 3) {}
  // var x;
  // assert x == blockIdx.x + 10;
  // foreach i < x {
  //   assert pow3(i) && i >= 1;
  // }

  for(int d = 500; d >= 1; d /= 4) {}
}
