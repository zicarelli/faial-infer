__global__ void uniform_add(int *data, int *partial_sums, int len) {
  __shared__ int buf[1];
  int id = ((blockIdx.x * blockDim.x) + threadIdx.x);

  if (id > len) return;

  if (threadIdx.x == 0) {
    buf[0] = partial_sums[blockIdx.x];
  }

  __syncthreads();
  data[id] += buf[0];
}

