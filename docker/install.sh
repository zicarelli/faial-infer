set -x
python3 -m pip install poetry==1.1.0 &&
poetry export --without-hashes -f requirements.txt --output requirements.txt &&
poetry build &&
python3 -m pip install pip-autoremove &&
pip-autoremove poetry -y &&
pip-autoremove pip-autoremove -y &&
python3 -m pip install -r requirements.txt &&
rm -f requirements.txt &&
python3 -m pip install --no-deps dist/*.whl