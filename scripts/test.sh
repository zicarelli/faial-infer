#!/bin/bash
result=0
for fname in examples/*.json; do
  if ! ./faial-infer "$fname" > /dev/null; then
    echo "ERROR: ./faial-infer $fname"
    ((result=result+1))
  fi
done
exit $result
